export const environment = {
    production: true
};

export const baseUrl = 'http://localhost:3500';

export const TABLE_PAGINATION_SIZE = 100;
