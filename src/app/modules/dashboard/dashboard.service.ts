import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { baseUrl } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private _httpClient: HttpClient) { }

  getuserDetails(): Observable<any[]> {
    const apiUrl = `${baseUrl}/User/GetUsers`;
    return this._httpClient.get<any[]>(apiUrl);
  }
  
  getuserbyId(Id: any): Observable<any[]> {              
    const apiUrl = `${baseUrl}/User/GetUsers/${Id}`;    
     return this._httpClient.get<any[]>(apiUrl);  
  }
}
