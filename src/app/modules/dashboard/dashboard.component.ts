import { Component, Inject, OnInit } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import {  FormControl, FormGroup,Validators, FormBuilder }  from '@angular/forms';
import { NgForm } from '@angular/forms';
import {Router,ActivatedRoute} from '@angular/router';
import { MD5 } from 'crypto-js';
import { DashboardService } from './dashboard.service';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  RegistrationId:any;
  roleIt: any;
  usersform : FormGroup;
  msg:string;
  name:string;
    constructor(private ds :DashboardService,private router:Router,private fb:FormBuilder,private activatedroute:ActivatedRoute ) { 
      var now = new Date();
      var hrs = now.getHours();
      var msg = "";
      
      if (hrs >  0) msg = "Good morning"; // REALLY early
      if (hrs >  6) msg = "Good morning";      // After 6am
      if (hrs > 12) msg = "Good afternoon";    // After 12pm
      if (hrs > 17) msg = "Good evening";      // After 5pm
      if (hrs > 22) msg = "Good evening";        // After 10pm
      this.msg=msg
    }
  
    ngOnInit() {
   console.log(this.router.navigateByUrl);
   this.activatedroute.data.subscribe(data => {
    console.log(data);
  })
      this.ds.getuserDetails().subscribe((data) => {
        console.log(data);
        this.name=data[0]['FirstName'];
       
      });
      this.ds.getuserbyId(this.RegistrationId).subscribe((data)=>{
        console.log(data);
        
      })
    //   this.ds.getuserbyId(RegistrationId:any).subscribe((data) => {
    //     this.roleIt= data[0]; 
    //     console.log(this.roleIt);
        
    //     // this.uomForm.get('Code').setValue(this.roleIt.Code);
    //     // this.uomForm.get('Name').setValue(this.roleIt.Name);
    //     // this.uomForm.get('Id').setValue(this.roleIt.Id);
    // });    
      // var obj={
      //   'username':this.loginform.get('Email')?.value,
      //   'password':
      //   MD5(
      //     this.loginform.get('password')?.value
      //   ).toString()
      // }
      // this.ls.validateuser().subscribe((data) => {
      //   console.log(data);
      //   this.router.navigate([`/dashboard`]);
      // }
    }
  
  }
  