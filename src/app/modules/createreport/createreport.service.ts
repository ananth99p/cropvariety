import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { baseUrl } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CreatereportService {

  constructor(private _httpClient: HttpClient) { }

  createreport(reportItems: any): Observable<any> {
    const apiUrl = `${baseUrl}/ReportGeneration/AddReportgeneration`;
    return this._httpClient.post<any>(apiUrl, reportItems);
  }
  }
  