import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {
  FormControl,
  FormGroup,
  Validators,
  FormBuilder,
} from '@angular/forms';
import { CreatereportService } from './createreport.service';

@Component({
  selector: 'app-createreport',
  templateUrl: './createreport.component.html',
  styleUrls: ['./createreport.component.scss']
})
export class CreatereportComponent implements OnInit {
  reportform: FormGroup;
  constructor(private router:Router,private fb: FormBuilder,private cs:CreatereportService) { }

  ngOnInit() {
    this.reportform = this.fb.group({
      Id: [null],
      ReportName: ['', Validators.required],
      StartDate: ['', Validators.required],
      EndDate: ['', Validators.required],
    	CreatedByUserId: [1],	
      UpdatedByUserId: [1],	
    });
  }
  next(){
    
    const ReportDetails: any = this.reportform.value;
this.cs.createreport(ReportDetails).subscribe((data)=>{
console.log(data);
this.router.navigate([`/importdis`]);
})
   
  }
}
