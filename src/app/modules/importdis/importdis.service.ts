import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { baseUrl } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ImportdisService {

  constructor(private _httpClient: HttpClient) { }

  getTemplateSubMenuDetails(): Observable<any[]> {
    const apiUrl = `${baseUrl}/Framework/GetTemplateSubMenuDetails`;
    return this._httpClient.get<any[]>(apiUrl);
  }
  getTemplateSubMenuItemDetails(): Observable<any[]> {
    const apiUrl = `${baseUrl}/Framework/GetTemplateSubMenuItemDetails`;
    return this._httpClient.get<any[]>(apiUrl);
  }
  getTemplateSubMenuItemDetailsById(Id:number): Observable<any[]> {              
    const apiUrl = `${baseUrl}/Framework/GetTemplateSubMenuItemDetailsById/${Id}`;   
     return this._httpClient.get<any[]>(apiUrl);
  }
  
  }
  