import { Component, OnInit } from '@angular/core';
import { ImportdisService } from './importdis.service';

@Component({
  selector: 'app-importdis',
  templateUrl: './importdis.component.html',
  styleUrls: ['./importdis.component.scss']
})
export class ImportdisComponent implements OnInit {
  morningSlotArr:any = [];
  cardMorningicon: string = "fa fa-plus";
  cardMorning: boolean = false;
  loading=true;
  templatedata:any;
  templateitemdata:any;
  checkboxes: any[]
  displaydata:boolean=false;
  name:string;
  constructor(private is:ImportdisService) {  }

  ngOnInit() {
    this.is.getTemplateSubMenuDetails().subscribe((data)=>{
      console.log(data);
      this.templatedata=data;
    })
    this.is.getTemplateSubMenuItemDetails().subscribe((data)=>{
      console.log(data);
      
    })
 
  
  }
  getdatabyId(Id:number){
    this.is.getTemplateSubMenuItemDetailsById(Id).subscribe((data)=>{
      console.log(data);
      
    })
  }
  showhideMorning(){
    if (this.cardMorning == false) {
      this.cardMorning = true;
      this.cardMorningicon = "fa fa-minus";
    }
    else {
      this.cardMorning = false;
      this.cardMorningicon = "fa fa-plus";
    }
    this.loading = false;
  }
  display(Id:any){
    this.is.getTemplateSubMenuItemDetailsById(Id).subscribe((data)=>{
      console.log(data);
      this.templateitemdata=data;
      this.displaydata=true;

    })


  }
  CheckAllOptions() {
    if (this.checkboxes.every(val => val.checked == true))
      this.checkboxes.forEach(val => { val.checked = false });
    else
      this.checkboxes.forEach(val => { val.checked = true });
  }
}
