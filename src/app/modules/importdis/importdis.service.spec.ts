/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ImportdisService } from './importdis.service';

describe('Service: Importdis', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ImportdisService]
    });
  });

  it('should ...', inject([ImportdisService], (service: ImportdisService) => {
    expect(service).toBeTruthy();
  }));
});
