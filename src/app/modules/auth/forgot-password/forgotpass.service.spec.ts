/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ForgotpassService } from './forgotpass.service';

describe('Service: Forgotpass', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ForgotpassService]
    });
  });

  it('should ...', inject([ForgotpassService], (service: ForgotpassService) => {
    expect(service).toBeTruthy();
  }));
});
