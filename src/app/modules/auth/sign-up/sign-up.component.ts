import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseAlertType } from '@fuse/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { SignUpService } from './sign-up.service';
import cryptoRandomString from 'crypto-random-string';
import { MD5 } from 'crypto-js';

@Component({
    selector     : 'auth-sign-up',
    templateUrl  : './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AuthSignUpComponent implements OnInit
{regform:FormGroup;
    constructor(private fb:FormBuilder,private router:Router,private ss:SignUpService) { }
  
    ngOnInit() {
      this.regform = this.fb.group({
        OrganizationName: ['', Validators.required],
        OrganizationType: ['', Validators.required],
        HeadOffice: ['', Validators.required],
        Country: ['', Validators.required],
        Pincode: [, Validators.required],
        ContactName: ['', Validators.required],
        EmailId: ['', [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
        ContactNumber: [, Validators.required],
        Designation: ['', Validators.required],
        Password:[''],
        EncryptedPassword:[''],
        
      });
    }
    Login(){
  
    }
    Register(){
      let Password=cryptoRandomString({length: 14});
  this.regform.get('Password')?.setValue(Password)
      var encryptedpassword = MD5(
       Password
      ).toString();
  this.regform.get('EncryptedPassword')?.setValue(encryptedpassword)
  
      const UserDetails: any = this.regform.value;
      console.log(UserDetails)
      this.ss.createuser(UserDetails).subscribe((Data)=>{
        console.log(Data);
        //alert('Thanks for Registration !! Your credentials have been sent to your Mail-Id');
      
      })
    }
  }
  