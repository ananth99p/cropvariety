/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { NewpassService } from './newpass.service';

describe('Service: Newpass', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewpassService]
    });
  });

  it('should ...', inject([NewpassService], (service: NewpassService) => {
    expect(service).toBeTruthy();
  }));
});
